pub mod adjts;
pub mod nouns;

pub const ADJTS: [&str; 1347] = self::adjts::ADJTS;
pub const NOUNS: [&str; 2327] = self::nouns::NOUNS;
