use std::fmt::Display;

#[derive(Debug, PartialEq)]
pub enum Error {
    NounNotFound,
    AdjtNotFound,
}

impl std::error::Error for Error {}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::NounNotFound => write!(
                f,
                "a suitable noun could not be found within the provided slice"
            ),
            Error::AdjtNotFound => write!(
                f,
                "a suitable adjt could not be found within the provided slice"
            ),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn should_return_message_for_noun() {
        let error = Error::NounNotFound;
        assert_eq!(
            error.to_string(),
            "a suitable noun could not be found within the provided slice"
        );
    }

    #[test]
    fn should_return_message_for_adjt() {
        let error = Error::AdjtNotFound;
        assert_eq!(
            error.to_string(),
            "a suitable adjt could not be found within the provided slice"
        );
    }
}
