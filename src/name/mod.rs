pub mod error;
pub mod options;
pub mod utils;

use std::fmt::Display;

use self::{error::Error, options::Options};

#[derive(Debug, PartialEq)]
pub struct Name<'a> {
    pub noun: &'a str,
    pub adjt: &'a str,
}

impl<'a, 'b> TryFrom<&mut Options<'a, 'b>> for Name<'b> {
    type Error = Error;

    fn try_from(options: &mut Options<'a, 'b>) -> Result<Self, Self::Error> {
        let nouns = options.nouns;

        let noun = utils::index(nouns, options.rand).ok_or(Error::NounNotFound)?;

        let adjts = &utils::retain(noun, options.adjts);

        let adjt = utils::index(adjts, options.rand).ok_or(Error::AdjtNotFound)?;

        Ok(Self { noun, adjt })
    }
}

impl Display for Name<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}-{}", self.adjt, self.noun)
    }
}

#[cfg(test)]
mod tests {
    use rand::rngs::mock::StepRng;

    use super::*;

    #[test]
    fn should_have_noun_from_options() {
        let mut options = Options {
            nouns: &["foo"],
            rand: &mut StepRng::new(0, 0),
            adjts: &["faz"],
        };

        let name = Name::try_from(&mut options).unwrap();

        assert_eq!(name.noun, "foo");
    }

    #[test]
    fn should_have_adjt_from_options() {
        let mut options = Options {
            nouns: &["foo"],
            rand: &mut StepRng::new(0, 0),
            adjts: &["faz"],
        };

        let name = Name::try_from(&mut options).unwrap();

        assert_eq!(name.adjt, "faz");
    }

    #[test]
    fn should_have_alliteration() {
        let mut options = Options {
            nouns: &["foo", "bar", "baz"],
            rand: &mut StepRng::new(0, 0),
            adjts: &["baz", "bar", "foo"],
        };

        let name = Name::try_from(&mut options).unwrap();

        assert_eq!(
            name.noun.chars().next().unwrap(),
            name.adjt.chars().next().unwrap(),
        );
    }

    #[test]
    fn should_use_rand_for_noun() {
        let mut options = Options {
            nouns: &["bar", "baz"],
            rand: &mut StepRng::new(1, 0),
            adjts: &["baz", "bar"],
        };

        let name = Name::try_from(&mut options).unwrap();

        assert_eq!(name.noun, "baz");
    }

    #[test]
    fn should_use_rand_for_adjt() {
        let mut options = Options {
            nouns: &["bar", "baz"],
            rand: &mut StepRng::new(1, 0),
            adjts: &["baz", "bar"],
        };

        let name = Name::try_from(&mut options).unwrap();

        assert_eq!(name.adjt, "bar");
    }

    #[test]
    fn should_return_err_for_none_nouns() {
        let mut options = Options {
            nouns: &[],
            rand: &mut StepRng::new(0, 0),
            adjts: &["foo"],
        };

        let name = Name::try_from(&mut options);

        assert_eq!(name, Err(Error::NounNotFound));
    }

    #[test]
    fn should_return_err_for_none_adjts() {
        let mut options = Options {
            nouns: &["foo"],
            rand: &mut StepRng::new(0, 0),
            adjts: &[],
        };

        let name = Name::try_from(&mut options);

        assert_eq!(name, Err(Error::AdjtNotFound));
    }

    #[test]
    fn should_return_string_with_format() {
        let name = Name {
            noun: "foo",
            adjt: "bar",
        };

        assert_eq!(name.to_string(), "bar-foo");
    }
}
