use rand::RngCore;

pub struct Options<'a, 'b> {
    pub nouns: &'a [&'b str],
    pub rand: &'a mut dyn RngCore,
    pub adjts: &'a [&'b str],
}
