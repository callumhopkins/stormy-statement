use rand::RngCore;

/// Checks to see if a noun-adjective pair is alliterative.
pub fn allit(noun: &str, adjt: &str) -> bool {
    let noun = match noun.chars().next() {
        Some(noun) => noun,
        None => return false,
    };

    let adjt = match adjt.chars().next() {
        Some(adjt) => adjt,
        None => return false,
    };

    noun == adjt
}

/// Retains only the adjectives that are alliterative with the noun.
pub fn retain<'a>(noun: &str, adjts: &[&'a str]) -> Vec<&'a str> {
    let mut vec = adjts.to_vec();

    vec.retain(|&adjt| allit(noun, adjt));

    vec
}

/// Selects a random item from the slice, using the given rng.
pub fn index<'a>(slice: &[&'a str], rand: &mut dyn RngCore) -> Option<&'a str> {
    let k = rand.next_u64() as usize;

    match k.checked_rem(slice.len()) {
        Some(index) => slice.get(index).copied(),
        None => Default::default(),
    }
}
